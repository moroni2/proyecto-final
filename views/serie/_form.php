<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Serie */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="serie-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_serie')->textInput() ?>

    <?= $form->field($model, 'nombre_serie')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'genero_serie')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'temporadas')->textInput() ?>

    <?= $form->field($model, 'estado')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comentarios_serie')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
