<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SerieSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="serie-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_serie') ?>

    <?= $form->field($model, 'nombre_serie') ?>

    <?= $form->field($model, 'genero_serie') ?>

    <?= $form->field($model, 'temporadas') ?>

    <?= $form->field($model, 'estado') ?>

    <?php // echo $form->field($model, 'comentarios_serie') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
