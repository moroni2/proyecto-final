<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = Yii::$app->name;
// Insertado carrousel y contenido principal
?>
<div class="site-index">

    <div class="body-content  ">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">

      <!-- Indicators -->

      <ol class="carousel-indicators">

        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>

        <li data-target="#myCarousel" data-slide-to="1"></li>

        <li data-target="#myCarousel" data-slide-to="2"></li>

      </ol>

      <div class="carousel-inner" role="listbox">

        <div class="item active">

            <img class="first-slide" src="img/bird box.png" alt="First slide">

          <div class="container">

            <div class="carousel-caption">

                           

            </div>

          </div>

        </div>

        <div class="item">

          <img class="second-slide" src="img/el irlandes.jpg" alt="Second slide">

          <div class="container">

            <div class="carousel-caption">

              
            </div>
              

          </div>

        </div>

        <div class="item">

          <img class="third-slide" src="img/mi primer beso 2.jpg" alt="Second slide">

          <div class="container">

            <div class="carousel-caption">

              
            </div>
              

          </div>

        </div>

        <div class="item">

          <img class="quarter-slide" src="img/ejercito de los muertos.jpg" alt="Third slide">

          <div class="container">

            <div class="carousel-caption">

                            

            </div>

          </div>

        </div>

      

      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">

        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>

        <span class="sr-only">Previous</span>

      </a>

      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">

        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>

        <span class="sr-only">Next</span>

      </a>

      </div>

    </div>
        <a href="site/vistapelicula" > 
            <div class="col-sm-3 ">
                <?= Html::img('../web/img/pelicula.jpg');?>
                <h3 style="text-align:center;">Peliculas</h3>
            </div>
        </a>
            <a href="site/vistaserie" > 
            <div class="col-sm-3 ">
                <?= Html::img('../web/img/serie.jpg');?>
                <h3 style="text-align:center;">Series</h3>
            </div>
                </a>
            <a href="lista/index" > 
                
                
            <div class="col-sm-3 ">
               <?= Html::img('../web/img/lista.jpg');?>
                <h3 style="text-align:center;">Listas</h3>
            </div>
                </a>
            <a href="usuario/index" > 
            <div class="col-sm-3 ">
                <?= Html::img('../web/img/usuario.png');?>
                <h3 style="text-align:center;">Usuarios</h3>
            </div>
                </a>
    </div>
</div>
