<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Peliculas';
?>

<div class="site-index">

<!-- Contiene contenido secundario de vistapelicula    -->

<div class=  "container">

        <div class="row ">
            <a href="../pelicula/view?id=1" > 
            <div class="col-lg-4 "> 
                
             
                
                 <?= Html::img('@web/img/pelicula.jpg');?>
             <h3>   España, la primera<br>globalización   </h3>
                
             




            </div>
           </a>
            
            <a href="../pelicula/view?id=2" >
            <div class=" col-lg-4  "  >
                
             
              <?= Html::img('@web/img/pelicula.jpg');?>
              
             <h3>   Libertad   </h3>
                
                


            </div>
            </a>
              
           <a href="../pelicula/view?id=3" >
            <div class="col-lg-4  ">
                
                
                <?= Html::img('@web/img/pelicula.jpg');?>
                
                <h3>   La rueda de la fortuna y<br>la fantasía  </h3>

                


            </div>
            </a>
        </div>
        
        <div class="row ">
            <a href="../pelicula/view?id=4" > 
            <div class="col-lg-4 "> 
                
             
                
                 <?= Html::img('@web/img/pelicula.jpg');?>
             <h3>   Petite maman   </h3>
                
             




            </div>
           </a>
            
            <a href="../pelicula/view?id=5" >
            <div class=" col-lg-4  "  >
                
             
              <?= Html::img('@web/img/pelicula.jpg');?>
              
             <h3>   El último duelo   </h3>
                
                


            </div>
            </a>
              
           <a href="../pelicula/view?id=6" >
            <div class="col-lg-4  ">
                
                
                <?= Html::img('@web/img/pelicula.jpg');?>
                
                <h3>   Dune  </h3>

                


            </div>
            </a>
        </div>

        <div class="row ">
            <a href="../pelicula/view?id=7" > 
            <div class="col-lg-4 "> 
                
             
                
                 <?= Html::img('@web/img/pelicula.jpg');?>
             <h3>   The Sparks Brothers   </h3>
                
             




            </div>
           </a>
            
            <a href="../pelicula/view?id=8" >
            <div class=" col-lg-4  "  >
                
             
              <?= Html::img('@web/img/pelicula.jpg');?>
              
             <h3>   Maixabel   </h3>
                
                


            </div>
            </a>
              
           <a href="../pelicula/view?id=9" >
            <div class="col-lg-4  ">
                
                
                <?= Html::img('@web/img/pelicula.jpg');?>
                
                <h3>   El buen patrón  </h3>

                


            </div>
            </a>
        </div>

        <div class="row">
            <a href="../pelicula/view?id=10" >
            <div class="col-lg-4 ">
                
                
                <?= Html::img('@web/img/pelicula.jpg');?>
                
                <h3>   Espiritu Sagrado   </h3>
                

                

                
            </div>
            </a>
            
            </div>
           
           
        </div>

    </div>


