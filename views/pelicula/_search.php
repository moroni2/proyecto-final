<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PeliculaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pelicula-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_pelicula') ?>

    <?= $form->field($model, 'nombre_pelicula') ?>

    <?= $form->field($model, 'genero_pelicula') ?>

    <?= $form->field($model, 'anio') ?>

    <?= $form->field($model, 'duracion') ?>

    <?php // echo $form->field($model, 'comentarios_pelicula') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
