<?php

return [
    'adminEmail' => 'moronicollazos4@gmail.com',
    'user.passwordResetTokenExpire' => 3600,
    'senderEmail' => 'moronicollazos4@gmail.com',
    'senderName' => 'filmy.com mailer',
    'supportEmail' => 'robot@devreadwrite.com'
];
