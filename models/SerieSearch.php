<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Serie;

/**
 * SerieSearch represents the model behind the search form of `app\models\Serie`.
 */
class SerieSearch extends Serie
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_serie', 'temporadas'], 'integer'],
            [['nombre_serie', 'genero_serie', 'estado', 'comentarios_serie'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Serie::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_serie' => $this->id_serie,
            'temporadas' => $this->temporadas,
        ]);

        $query->andFilterWhere(['like', 'nombre_serie', $this->nombre_serie])
            ->andFilterWhere(['like', 'genero_serie', $this->genero_serie])
            ->andFilterWhere(['like', 'estado', $this->estado])
            ->andFilterWhere(['like', 'comentarios_serie', $this->comentarios_serie]);

        return $dataProvider;
    }
}
