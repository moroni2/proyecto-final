<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Lista;

/**
 * ListaSearch represents the model behind the search form of `app\models\Lista`.
 */
class ListaSearch extends Lista
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_lista'], 'integer'],
            [['username', 'nombre_pelicula', 'nombre_serie'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Lista::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_lista' => $this->id_lista,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'nombre_pelicula', $this->nombre_pelicula])
            ->andFilterWhere(['like', 'nombre_serie', $this->nombre_serie]);

        return $dataProvider;
    }
}
