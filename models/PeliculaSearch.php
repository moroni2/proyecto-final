<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pelicula;

/**
 * PeliculaSearch represents the model behind the search form of `app\models\Pelicula`.
 */
class PeliculaSearch extends Pelicula
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pelicula', 'anio'], 'integer'],
            [['nombre_pelicula', 'genero_pelicula', 'duracion', 'comentarios_pelicula'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pelicula::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pelicula' => $this->id_pelicula,
            'anio' => $this->anio,
        ]);

        $query->andFilterWhere(['like', 'nombre_pelicula', $this->nombre_pelicula])
            ->andFilterWhere(['like', 'genero_pelicula', $this->genero_pelicula])
            ->andFilterWhere(['like', 'duracion', $this->duracion])
            ->andFilterWhere(['like', 'comentarios_pelicula', $this->comentarios_pelicula]);

        return $dataProvider;
    }
}
