<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "serie".
 *
 * @property int $id_serie
 * @property string|null $nombre_serie
 * @property string $genero_serie
 * @property int $temporadas
 * @property string $estado
 * @property string|null $comentarios_serie
 *
 * @property Lista[] $listas
 */
class Serie extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'serie';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_serie', 'genero_serie', 'temporadas', 'estado'], 'required'],
            [['id_serie', 'temporadas'], 'integer'],
            [['nombre_serie', 'genero_serie', 'comentarios_serie'], 'string', 'max' => 1000],
            [['estado'], 'string', 'max' => 10],
            [['nombre_serie'], 'unique'],
            [['id_serie'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_serie' => 'Id Serie',
            'nombre_serie' => 'Nombre Serie',
            'genero_serie' => 'Genero Serie',
            'temporadas' => 'Temporadas',
            'estado' => 'Estado',
            'comentarios_serie' => 'Comentarios Serie',
        ];
    }

    /**
     * Gets query for [[Listas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getListas()
    {
        return $this->hasMany(Lista::className(), ['nombre_serie' => 'nombre_serie']);
    }
}
