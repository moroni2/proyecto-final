<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lista".
 *
 * @property int $id_lista
 * @property string $username
 * @property string|null $nombre_pelicula
 * @property string|null $nombre_serie
 *
 * @property Pelicula $nombrePelicula
 * @property Serie $nombreSerie
 * @property User $username0
 */
class Lista extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lista';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'nombre_pelicula', 'nombre_serie'], 'required'],
            [['username'], 'string', 'max' => 255],
            [['nombre_pelicula', 'nombre_serie'], 'string', 'max' => 40],
            [['nombre_pelicula'], 'exist', 'skipOnError' => true, 'targetClass' => Pelicula::className(), 'targetAttribute' => ['nombre_pelicula' => 'nombre_pelicula']],
            [['nombre_serie'], 'exist', 'skipOnError' => true, 'targetClass' => Serie::className(), 'targetAttribute' => ['nombre_serie' => 'nombre_serie']],
            [['username'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['username' => 'username']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_lista' => 'Id Lista',
            'username' => 'Username',
            'nombre_pelicula' => 'Nombre Pelicula',
            'nombre_serie' => 'Nombre Serie',
        ];
    }

    /**
     * Gets query for [[NombrePelicula]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombrePelicula()
    {
        return $this->hasOne(Pelicula::className(), ['nombre_pelicula' => 'nombre_pelicula']);
    }

    /**
     * Gets query for [[NombreSerie]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreSerie()
    {
        return $this->hasOne(Serie::className(), ['nombre_serie' => 'nombre_serie']);
    }

    /**
     * Gets query for [[Username0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsername0()
    {
        return $this->hasOne(User::className(), ['username' => 'username']);
    }
}
