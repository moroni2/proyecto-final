<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pelicula".
 *
 * @property int $id_pelicula
 * @property string|null $nombre_pelicula
 * @property string $genero_pelicula
 * @property int $anio
 * @property string $duracion
 * @property string|null $comentarios_pelicula
 *
 * @property Lista[] $listas
 */
class Pelicula extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pelicula';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pelicula', 'genero_pelicula', 'anio', 'duracion'], 'required'],
            [['id_pelicula', 'anio'], 'integer'],
            [['nombre_pelicula', 'genero_pelicula', 'comentarios_pelicula'], 'string', 'max' => 1000],
            [['duracion'], 'string', 'max' => 8],
            [['nombre_pelicula'], 'unique'],
            [['id_pelicula'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_pelicula' => 'Id Pelicula',
            'nombre_pelicula' => 'Nombre Pelicula',
            'genero_pelicula' => 'Genero Pelicula',
            'anio' => 'Anio',
            'duracion' => 'Duracion',
            'comentarios_pelicula' => 'Comentarios Pelicula',
        ];
    }

    /**
     * Gets query for [[Listas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getListas()
    {
        return $this->hasMany(Lista::className(), ['nombre_pelicula' => 'nombre_pelicula']);
    }
}
