-- MySQL dump 10.16  Distrib 10.1.40-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: proyecto2
-- ------------------------------------------------------
-- Server version	10.1.40-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

DROP DATABASE IF EXISTS `proyecto2`;
CREATE DATABASE `proyecto2`;
USE `proyecto2`;
--
-- Table structure for table `lista`
--

DROP TABLE IF EXISTS `lista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lista` (
  `id_lista` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_pelicula` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre_serie` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_lista`),
  KEY `nombre_pelicula` (`nombre_pelicula`),
  KEY `nombre_serie` (`nombre_serie`),
  KEY `username` (`username`),
  CONSTRAINT `lista_ibfk_1` FOREIGN KEY (`nombre_pelicula`) REFERENCES `pelicula` (`nombre_pelicula`),
  CONSTRAINT `lista_ibfk_2` FOREIGN KEY (`nombre_serie`) REFERENCES `serie` (`nombre_serie`),
  CONSTRAINT `lista_ibfk_3` FOREIGN KEY (`username`) REFERENCES `user` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lista`
--

LOCK TABLES `lista` WRITE;
/*!40000 ALTER TABLE `lista` DISABLE KEYS */;
INSERT INTO `lista` VALUES (3,'user','España, la primera globalización','Black Mirror'),(4,'user','El último duelo','True Detective'),(7,'user','El buen patrón','Fargo');
/*!40000 ALTER TABLE `lista` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1637767126),('m211119_131036_create_user_table',1637767128);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pelicula`
--

DROP TABLE IF EXISTS `pelicula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pelicula` (
  `id_pelicula` int(3) NOT NULL,
  `nombre_pelicula` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `genero_pelicula` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `anio` int(4) NOT NULL,
  `duracion` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `comentarios_pelicula` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_pelicula`),
  UNIQUE KEY `nombre_pelicula` (`nombre_pelicula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pelicula`
--

LOCK TABLES `pelicula` WRITE;
/*!40000 ALTER TABLE `pelicula` DISABLE KEYS */;
INSERT INTO `pelicula` VALUES (1,'España, la primera globalización','Documental',2021,'110 min.','\'España, la primera globalización\' pone el foco en defender y divulgar hechos ciertos de nuestra compleja y emocionante historia. El documental desmonta las mentiras de una operación de propaganda tan eficaz en el pasado, que ha conseguido que los propios descendientes de aquellos españoles de la península y de América la hayamos interiorizado. Ofrece nuevas lecturas sobre el período histórico iniciado en el reinado de los Reyes Católicos, el descubrimiento de América y el posterior devenir de la historia de España, desmontando la leyenda negra en un momento tan necesario como el actual.'),(2,'Libertad','Drama',2021,'104 min.','La familia Vidal pasa en su casa de verano las últimas vacaciones de la abuela Ángela, que sufre Alzheimer avanzado. Por primera vez en su vida, Nora, de 14 años, siente que no encuentra su lugar. Los juegos de niños le parecen ridículos y las conversaciones de los adultos todavía le van grandes. Pero todo cambia con la llegada de Libertad, de 15 años e hija de Rosana, la mujer colombiana que cuida a Ángela. Rebelde y magnética, Libertad se convierte en la puerta de entrada a un verano distinto para Nora, y las dos chicas rápidamente forjan una amistad intensa y desigual. Juntas salen de la burbuja de protección y confort que supone la casa familiar, descubriendo un mundo nuevo en el que Nora se siente más libre que nunca. (FILMAFFINITY)'),(3,'La rueda de la fortuna y la fantasía','Drama',2021,'121 min.','Contada en tres movimientos, es una colección de historias protagonizadas por personajes femeninos que trazan las trayectorias entre sus elecciones y arrepentimientos. Un triángulo amoroso inesperado, una trampa de seducción fallida y un encuentro que resulta de un malentendido.'),(4,'Petite maman','Drama',2021,'72 min.','Nelly tiene 8 años y acaba de perder a su abuela. Mientras ayuda a sus padres a vaciar la casa en la que su madre creció, explora intrigada el bosque que la rodea, donde su mamá solía jugar de pequeña. Allí Nelly conoce a otra niña de su edad, y la inmediata conexión entre ambas da paso a una preciosa amistad. Juntas construyen una cabaña en el bosque y, entre juegos y confidencias, desvelarán un fascinante secreto. (FILMAFFINITY)'),(5,'El último duelo','Drama',2021,'152 min.','Basada en hechos reales. Francia, 1386. Cuenta el enfrentamiento entre el caballero Jean de Carrouges (Matt Damon) y el escudero Jacques LeGris (Adam Driver), al acusar el primero al segundo de abusar de su esposa, Marguerite de Carrouges (Jodie Comer). El Rey Carlos VI decide que la mejor forma de solucionar el conflicto es un duelo a muerte. El que venza será el ganador, sin embargo, si lo hace el escudero, la esposa del caballero será quemada como castigo por falsas acusaciones.'),(6,'Dune','Ciencia ficción',2021,'155 min.','Arrakis, el planeta del desierto, feudo de la familia Harkonnen desde hace generaciones, queda en manos de la Casa de los Atreides después de que el emperador ceda a ésta la explotación de las reservas de especia, una de las materias primas más valiosas de la galaxia y también una droga capaz de amplificar la conciencia y extender la vida. El duque Leto (Oscar Isaac), la dama Jessica (Rebecca Ferguson) y el hijo de ambos, Paul Atreides (Timothée Chalamet), llegan al planeta con la esperanza de recuperar el renombre de su casa, pero pronto se verán envueltos en una trama de traiciones y engaños que les llevarán a cuestionar su confianza entre sus más allegados y a valorar a los lugareños, los Fremen, una estirpe de habitantes del desierto con una estrecha relación con la especia.'),(7,'The Sparks Brothers','Documental',2021,'141 min.','Sparks es la banda favorita de tu banda favorita, y pronto será también la tuya. Lo sepas o no, Sparks ha tenido algo que ver en algo que te encanta. Ellos han formado parte de prácticamente toda forma artística de los últimos 50 años. Hijos de la década de los años 60, Ron y Russell son dos hermanos de Los Ángeles que se alimentaban a base de palomitas de maíz y música pop hasta que el fulgor de los espectáculos musicales iluminaron un camino que ha dado como fruto 25 álbumes de estudio. (FILMAFFINITY)'),(8,'Maixabel','Drama',2021,'115 min.','Maixabel Lasa pierde en el año 2000 a su marido, Juan María Jaúregui, asesinado por ETA. Once años más tarde, recibe una petición insólita: uno de los asesinos ha pedido entrevistarse con ella en la cárcel de Nanclares de la Oca /Álava), en la que cumple condena tras haber roto sus lazos con la banda terrorista. A pesar de las dudas y del inmenso dolor, Maixabel accede a encontrarse cara a cara con las personas que acabaron a sangre fría con la vida de quien había sido su compañero desde los dieciséis años.'),(9,'El buen patrón','Comedia',2021,'120 min.','Julio Blanco, el carismático propietario de una empresa que fabrica balanzas industriales en una ciudad española de provincias, espera la inminente visita de una comisión que decidirá la obtención de un premio local a la excelencia empresarial. Todo tiene que estar perfecto para la visita. Sin embargo, todo parece conspirar contra él. Trabajando a contrarreloj, Blanco intenta resolver los problemas de sus empleados, cruzando para ello todas las líneas imaginables, y dando lugar a una inesperada y explosiva sucesión de acontecimientos de imprevisibles consecuencias.'),(10,'Espiritu Sagrado','Comedia',2021,'97 min.','Julio está muerto. Es una noticia terrible para OVNI Levante, la asociación de aficionados a la ufología que él dirigió. La muerte le golpea con especial fuerza a uno de sus miembros, José Manuel. Julio y él tenían un proyecto común de vital importancia. Ahora deberá continuar solo como único conocedor del secreto cósmico que puede alterar el porvenir humano. Mientras tanto, en España se busca a una niña que desapareció hace semanas. (FILMAFFINITY)');
/*!40000 ALTER TABLE `pelicula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `serie`
--

DROP TABLE IF EXISTS `serie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `serie` (
  `id_serie` int(3) NOT NULL,
  `nombre_serie` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `genero_serie` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `temporadas` int(2) NOT NULL,
  `estado` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `comentarios_serie` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_serie`),
  UNIQUE KEY `nombre_serie` (`nombre_serie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `serie`
--

LOCK TABLES `serie` WRITE;
/*!40000 ALTER TABLE `serie` DISABLE KEYS */;
INSERT INTO `serie` VALUES (1,'The corner','Miniserie de TV',1,'Finalizada','Miniserie de la HBO sobre el mundo de la droga. Describe la vida de una familia de clase media de Baltimore, que vive hundida en la miseria por causa de la adicción a la heroína. Los guionistas David Simon y Edward Burns son los mismos de la serie \"The Wire\". (FILMAFFINITY)'),(2,'Black Mirror','Serie de TV',5,'Finalizada','Serie antológica creada por Charlie Brooker (\"Dead Set\") que muestra el lado oscuro de la tecnología y cómo esta afecta y puede alterar nuestra vida, a veces con consecuencias tan impredecibles como aterradoras. \"Black Mirror\" comenzó su emisión en 2011 en el canal británico Channel 4, con dos temporadas de tres episodios cada una, y tras producirse un especial de Navidad la serie fue comprada y renovada por Netflix, que ya ha producido tres temporadas más. (FILMAFFINITY)'),(3,'Fargo','Miniserie de TV',4,'Finalizada','Lester Nygaard (Martin Freeman), un apocado vendedor de seguros de una pequeña población de Minnesota, ve cómo su mundo cambia por completo con la llegada de un misterioso y salvaje desconocido (Billy Bob Thornton). Serie basada en la película homónima de los hermanos Coen, aunque presenta personajes diferentes y se ambienta en 2006. (FILMAFFINITY)'),(4,'Romanzo criminale','Serie de TV',2,'Finalizada','Adaptación de la popular novela \'Romanzo criminale\'. Narra el ascenso y caída de la banda de la Magliana, integrada por un grupo de jóvenes delincuentes que dominaron el narcotráfico en Roma a finales de los 70, llegando a establecer conexiones con la Mafia, la Camorra e incluso los Servicios Secretos Italianos. (FILMAFFINITY)'),(5,'True Detective','Serie de TV',3,'Finalizada','Dos detectives de Lousiana, Rust Cohle (Matthew McConaughey) y Martin Hart (Woody Harrelson), vuelven a investigar el difícil caso de un asesino en serie en el que ya habían trabajado. Obligados a regresar a un mundo tan siniestro, el avance de la investigación y el mayor conocimiento mutuo les enseñan que la oscuridad reside a ambos lados de la ley. (FILMAFFINITY)'),(6,'Chernobyl','Miniserie de TV',1,'Finalizada','El 26 de abril de 1986, la Central Nuclear de Chernóbil, en Ucrania (por entonces perteneciente a la Unión Soviética), sufrió una explosión masiva que liberó material radioactivo en Ucrania, Bielorrusia, Rusia, así como en zonas de Escandinavia y Europa Central. La serie relata, desde múltiples puntos de vista, lo que aconteció en torno a una de las mayores tragedias en la historia reciente, así como los sacrificios realizados para salvar al continente de un desastre sin precedentes. (FILMAFFINITY)'),(7,'Hermanos de sangre','Miniserie de TV',1,'Finalizada','Narra la historia de la Easy Company, un batallón estadounidense del regimiento 506 de paracaidistas, que luchó en Europa durante la II Guerra Mundial. Incluye entrevistas a los supervivientes, recuerdos de los periodistas y cartas de los soldados... Basada en el bestseller \"Band Of Brothers\", de Stephen E. Ambrose.'),(8,'Juego de tronos','Serie de TV',8,'Finalizada','La historia se desarrolla en un mundo ficticio de carácter medieval donde hay Siete Reinos. Hay tres líneas argumentales principales: la crónica de la guerra civil dinástica por el control de Poniente entre varias familias nobles que aspiran al Trono de Hierro; la creciente amenaza de \"los otros\", seres desconocidos que viven al otro lado de un inmenso muro de hielo que protege el Norte de Poniente; y el viaje de Daenerys Targaryen, la hija exiliada del rey que fue asesinado en una guerra civil anterior, y que pretende regresar a Poniente para reclamar sus derechos dinásticos. Tras un largo verano de varios años, el temible invierno se acerca a los Siete Reinos. Lord Eddard \'Ned\' Stark, señor de Invernalia, deja sus dominios para ir a la corte de su amigo, el rey Robert Baratheon, en Desembarco del Rey, la capital de los Siete Reinos. Stark se convierte en la Mano del Rey e intenta desentrañar una maraña de intrigas que pondrá en peligro su vida y la de todos los suyos. Mientras tanto,'),(9,'Breaking Bad','Serie de TV',5,'Finalizada','Tras cumplir 50 años, Walter White (Bryan Cranston), un profesor de química de un instituto de Albuquerque, Nuevo México, se entera de que tiene un cáncer de pulmón incurable. Casado con Skyler (Anna Gunn) y con un hijo discapacitado (RJ Mitte), la brutal noticia lo impulsa a dar un drástico cambio a su vida: decide, con la ayuda de un antiguo alumno (Aaron Paul), fabricar anfetaminas y ponerlas a la venta. Lo que pretende es liberar a su familia de problemas económicos cuando se produzca el fatal desenlace. (FILMAFFINITY)'),(10,'The wire (Bajo escucha)','Serie de TV',5,'Finalizada','En los barrios bajos de Baltimore, se investiga un asesinato relacionado con el mundo de las drogas. Un policía es el encargado de detener a los miembros de un importante cártel. La corrupción policial, las frágiles lealtades dentro de los cárteles y la miseria vinculada al narcotráfico son algunos de los problemas denunciados en esta serie. Parece inspirarse en series modernas como \"Los Soprano\" y en clásicos como \"Policías de Nueva York (NYPD Blue)\". El creador de la serie fue, durante años, reportero de la crónica negra de Baltimore, y uno de los coguionistas fue policía en la misma ciudad. (FILMAFFINITY)');
/*!40000 ALTER TABLE `serie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (2,'user','sSj-62gpn7k9_YL4j1nugof2eFaKQ8So','$2y$13$6SCKaiyggLttOaTXsLVs1.d9zhJyXldwS2.3lZK3JxVsHojai8xLe',NULL,'user@mail.com',10,1637926428,1637926428),(3,'admin','ZA4-GRLmW1pseGt0051wAsl-vCOdASSc','$2y$13$UZ/jxpGwTygKC1/NatbqoOWEMP4VuuipgsTJ7nyrf1vFuwahQpCtW','EyGMbM_VlhJOBvd66jDMOdRy5YQ8qdsK_1638282051','moronicollazos4@gmail.com',10,1638187254,1638282051);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-01 15:20:11
