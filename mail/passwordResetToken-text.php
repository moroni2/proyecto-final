<?php
$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
// Texto que se envia al correo electronico
?>
Hola <?= $user->username ?>,
Siga el enlace a continuación para restablecer su contraseña:
<?= $resetLink ?>